package collection;

import java.util.LinkedList;
import java.util.Queue;

public class QueueDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Queue<Integer> numbers = new LinkedList<>();
		
		numbers.offer(100);
		numbers.offer(101);
		numbers.offer(102);
		numbers.offer(103);
		numbers.offer(104);
		
		int somename = numbers.peek();
		System.out.println("The head of the queue is : " +somename);
		
		System.out.println(numbers);
		
		int removednumber = numbers.poll();
		System.out.println("The removed no is : " +removednumber);
		
		System.out.println(numbers);
		

	}

}
