package collection;
import java.util.List;
import java.util.ArrayList;

public class Demo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> list = new ArrayList<>();
		
		list.add("Namjoon");
		list.add("Seokjin");
		list.add("Yoongi");
		list.add("Jimin");
		list.add("Taehyung");
		System.out.println(list); 
		list.add("Jungkook");
		list.add(3, "Hoseok");
		list.set(0, "BTS");
		list.add(1, "Namjoon");
		System.out.println(list);
		
		List<String> idols = new ArrayList<>();
		
		idols.add("Soobin");
		idols.add("Yeonjun");
		idols.add("Beomgyu");
		idols.add("Taehyun");
		idols.add("Hueningkai");
		idols.set(0, "TXT");
		idols.add(1, "Soobin");
		System.out.println(idols);
	
		int tsize = list.size();
		System.out.println(tsize);
		
		idols.addAll(5, list);
		System.out.println("After merging list with idols:  "   +idols);
		

		int ssize = idols.size();
		System.out.println(ssize);
		
		
		
	}

}
