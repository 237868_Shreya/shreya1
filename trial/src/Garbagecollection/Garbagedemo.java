package Garbagecollection;

public class Garbagedemo {
	public void finalize() {
		System.out.println("unreferenced object");
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		

		Garbagedemo gd = new Garbagedemo();
		gd=null; //nullifying
		Garbagedemo gd2 = new Garbagedemo();
		gd = gd2; // refering other
		new Garbagedemo();
		System.gc();

	}

}
